/*****************************************************************************
 * Dies ist die abgewandelte Form der dft.c fuer das "Applaus Meter" Projekt *
 *****************************************************************************/
#include "LabCode.h"
#include "FFT/fft.h"
#include <math.h>
#ifdef LAB5_DFT

#define M FFT_SIZE

// typedef to have complex data type
typedef struct{
  float32_t real;
  float32_t imag;
} COMPLEX;

float32_t fft_buffer[BUFFER_SIZE];
COMPLEX result[M];
COMPLEX *x = fft_buffer;
COMPLEX twiddle[M];

/************************************************************************
 * Die INIT aus dem Praktikum, um nur einmal cosf und sinf zu berechnen *
 ************************************************************************/
void Init_FFT(){
	int32_t n;
	float32_t invM = 1.0/((float32_t)(M));

	for(n=0 ; n<M ; n++){
	    twiddle[n].real = cosf(2*PI*n*invM);
	    twiddle[n].imag = sinf(2*PI*n*invM);
	}
}

/****************************
 * Zuerst die FFT berechnen *
 ****************************/
void FFT(){

  int k,n;
  float32_t invM = 1.0/((float32_t)(M));
  float32_t w;

  for (k=0 ; k<M ; k++){
    result[k].real = 0.0f;
    result[k].imag = 0.0f;

    for (n=0 ; n<M ; n++){

      // Argument to cosine and sine function
      w = 2.0*PI*k*n*invM;
      // Complete the following two code lines:
      result[k].real += x[n].real * twiddle[((n*k)%M)].real - x[n].imag * twiddle[((n*k)%M)].imag;
      result[k].imag += x[n].real * twiddle[((n*k)%M)].imag + x[n].imag * twiddle[((n*k)%M)].real;

      //result[k].real += x[n].real * cosf(w) - x[n].imag * sinf(w);
      //result[k].imag += x[n].real * sinf(w) + x[n].imag * cosf(w);
    }
  }

  for (k=0 ; k<M ; k++){
    x[k] = result[k];
  }

/******************
 * Filter ab hier *
 ******************/
/*
  //#define N_IIR 7
  //Das sind die Koeffizienten fur den Filter
  float32_t b[7] = { 0.004870811509,-0.001725586717, 0.008795080542, -0.000563848145, 0.008795080542,-0.001725586717, 0.004870811509};
  float32_t a[7] = { 1.000000000000,-4.201166610316, 8.143251320212, -9.099574323047, 6.139046694375,-2.363090332875, 0.406231619213};

  COMPLEX wL[7];//Siehe Filteraufbau

  int16_t e; // index for filter coefficients
  float32_t g; // output

  // initialize new value for state signal with new input sample
  wL[0] = x[0];//
  // initialize new value for output signal
  g = 0.0f;
  //Compute samples from left to right, i.e. start with k = N_IIR - 1
  for (e = 7-1; e>0 ; e--){

      // compute new value for state signal using a[k]
      wL[0] -= a[e] * wL[e];
      // compute new value for output signal using b[k]
      g +=  wL[e] * b[e];
      // shift wL[k-1] to wL[k]
      wL[e] = wL[e-1];
  }

  // k=0 still needs to be handled, since we started on the left (k=N_IIR-1)
  // we can immediately use the value for wL[0] computed by the loop above
  g += wL[0] * b[0];

  x = (int32_t)(g);
  //output the input signal from the left channel on the right channel of
  //the output to be able to compare the input and output signal of the filter
  //--*outRight = inLeft;
  // circularly increment interrupt counter
  if (++interrupt_counter == CM){
      interrupt_counter = 0;
  }
    cycles_btwn_irs[interrupt_counter] = 0;
    */
  }


void Copy_To_FFT_Buffer(int32_t* buffer){
  // Copy from processing buffer to fft_buffer and convert from int32_t
  // to float32_t. q31 is an ARM CMSIS DSP fractional type equivalent to int32_t
  // but results are scaled by 1/(2**31). Thus resulting floating point values
  // are between -1.0 and 1.0
  arm_q31_to_float(buffer, fft_buffer, BUFFER_SIZE);
}

#endif
