// dftw.c 
//
// M-point DFTW
//
#include "LabCode.h"
#include "FFT/fft.h"
#include <math.h>
#ifdef LAB5_DFTW
#define M FFT_SIZE


//#define PI 3.14159265358979

typedef struct
{
  float32_t real;
  float32_t imag;
} COMPLEX;
float32_t fft_buffer[BUFFER_SIZE];//fft input and output buffer
// local buffer for results of the DFT
COMPLEX result[M];
// define pointer to fft_buffer
COMPLEX *x = fft_buffer;
COMPLEX twiddle[M];

void Init_FFT()
{
  int32_t n;
  float32_t invM = 1.0/((float32_t)(M));
  for(n=0 ; n<M ; n++)
  {
    twiddle[n].real = cosf(2*PI*n*invM);
    twiddle[n].imag = sinf(2*PI*n*invM);
  }
}


void FFT()
{
  int32_t n,k;
  for (k=0 ; k<M ; k++)
  {
    result[k].real = 0.0f;
    result[k].imag = 0.0f;

    for (n=0 ; n<M ; n++)
    {
      // Complete the following two code lines:
      result[k].real += x[n].real * twiddle[(n%M)].real + x[n].imag * twiddle[(n%M)].imag;
      result[k].imag += x[n].real * twiddle[(n%M)].real + x[n].imag * twiddle[(n%M)].real;
    }
  }

  for (k=0 ; k<M ; k++)
  {
    x[k] = result[k];
  }
}

void Copy_To_FFT_Buffer(int32_t* buffer)
{
  // Copy from processing buffer to fft_buffer and convert from int32_t
  // to float32_t. q31 is an ARM CMSIS DSP fractional type equivalent to int32_t
  // but results are scaled by 1/(2**31). Thus resulting floating point values
  // are between -1.0 and 1.0
  arm_q31_to_float(buffer, fft_buffer, BUFFER_SIZE);
}
#endif
